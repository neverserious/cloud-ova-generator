#!/bin/bash -eux

abort() {
    echo -e >&2 "Something went wrong..."
    echo -e "$1" >&2
    exit 1
}

for i in "$@"; do
  case $i in
    -D=*|--distro=*)
      export distro="${i#*=}"
      shift
    ;;
    -v|--verbose)
      export DIB_DEBUG_TRACE=1
      shift
    ;;
    *)
      abort "Unknown option on command line. "
    ;;
  esac
done
scratchd=$(mktemp -d)

# Perform some sanity checking
required_bin="tar sudo python3 wget qemu-img"
for binary in ${required_bin}; do
  which ${binary} >/dev/null 2>&1 || abort "Missing binary: ${binary}, please install that first"
done

convert () {
  distro=${1}
  source distros.d/${1}.sh || abort "Distro ${1} not found"
  echo $name
  scratchd=$(mktemp -d)
  curl -sL -o ${scratchd}/sums ${sums} || abort "Failed to download sums"
  curl -sL -o ${scratchd}/${distro} ${image} || abort "Failed to download ${distro} image"
  prefix="${shortname}-${version}-cloud-${arch}"
  vmdk_f="${scratchd}/${prefix}.vmdk"
  qemu-img convert -f ${format} -O vmdk -o adapter_type=lsilogic,subformat=streamOptimized,compat6 \
    ${scratchd}/${distro} \
    ${vmdk_f} || abort "Failed to convert image to vmdk"
  
  # Get information about the VMDK
  vmdk_size=$(du -b "${vmdk_f}" | cut -f1)
  vmdk_capacity=$(qemu-img info "${vmdk_f}" | awk '-F[\( ]' '$1 ~ /virtual/ && $NF ~ /bytes.*/ {print$(NF-1)}')

  # Populate the OVF template
  ovf_template="ova.tmpl"
  ovf="${scratchd}/${prefix}.ovf"
  serial=$(date +%Y%m%d)
  cp "${ovf_template}" "${ovf}"
  sed -i "${ovf}" \
    -e "s/@@NAME@@/${name}/g" \
    -e "s/@@FILENAME@@/${vmdk_f##*/}/g" \
    -e "s/@@VMDK_FILE_SIZE@@/${vmdk_size}/g" \
    -e "s/@@VMDK_CAPACITY@@/${vmdk_capacity}/g" \
    -e "s/@@NUM_CPUS@@/${cpus:-2}/g" \
    -e "s/@@VERSION@@/${version}/g" \
    -e "s/@@DATE@@/${serial}/g" \
    -e "s/@@MEM_SIZE@@/${memory:-1024}/g" \
    -e "s/@@OVF_ID@@/${ovf_id}/g" \
    -e "s/@@OVF_OS_TYPE@@/${ovf_os_type}/g" \
    -e "s/@@OVF_DESC_BITS@@/${ovf_desc_bits}/g" \
    -e "s/@@VMX@@/${vmx_version}/g;"

  # Get the checksums
  vmdk_sha256=$(sha256sum ${vmdk_f} | cut -d' ' -f1)
  ovf_sha256=$(sha256sum ${ovf} | cut -d' ' -f1)

  # Generate the manifest
  manifest="${scratchd}/${prefix}.mf"
  cat <<EOF >> "${manifest}"
SHA256(${vmdk_f##*/})= ${vmdk_sha256}
SHA256(${ovf##*/}.ovf)= ${ovf_sha256}
EOF

  tar -C ${scratchd} -cf ${prefix}-${serial}.ova \
    ${prefix}.ovf \
    ${prefix}.mf \
    ${vmdk_f##*/}

  # Clean up our mess
  rm -rf ${scratchd}
}

if [ "${distro}" == "" ]; then
  abort "Required argument missing: --distro"
else
  convert $distro
fi

